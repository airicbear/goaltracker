import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:goal_tracker/record.dart';

class GoalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('goals').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();
        return _buildList(context, snapshot.data.documents);
      },
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    snapshot.sort((a, b) {
      Record recordA = Record.fromSnapshot(a);
      Record recordB = Record.fromSnapshot(b);
      return recordA.date.compareTo(recordB.date);
    });
    return ListView(
        padding: const EdgeInsets.only(top: 20.0),
        children:
            snapshot.map((data) => _buildListItem(context, data)).toList());
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
    final record = Record.fromSnapshot(data);
    final _isOverdue = record.date.isBefore(DateTime.now());

    String _date() {
      if (record.date.difference(DateTime.now()).inDays < 1 &&
          record.date.isAfter(DateTime.now().subtract(Duration(days: 1)))) {
        return MaterialLocalizations.of(context)
            .formatTimeOfDay(TimeOfDay.fromDateTime(record.date));
      } else if (record.date.difference(DateTime.now()).inDays < 7) {
        return MaterialLocalizations.of(context).formatMediumDate(record.date);
      }
      return MaterialLocalizations.of(context).formatCompactDate(record.date);
    }

    return Padding(
      key: ValueKey(record.title),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: ListTile(
          tileColor: Theme.of(context).hoverColor,
          title: Text(
            record.title,
            style: _isOverdue
                ? TextStyle(color: Theme.of(context).disabledColor)
                : TextStyle(color: Theme.of(context).textTheme.button.color),
          ),
          trailing: Text(
            _date(),
            style: _isOverdue
                ? TextStyle(color: Theme.of(context).disabledColor)
                : TextStyle(color: Theme.of(context).textTheme.button.color),
          ),
          onTap: () {
            record.reference.delete();
          },
        ),
      ),
    );
  }
}
