import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:goal_tracker/ad_manager.dart';
import 'package:goal_tracker/create_goal_page.dart';
import 'package:goal_tracker/goal_list.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Goal Tracker',
      darkTheme: ThemeData.dark(),
      home: MyHomePage(title: 'Goal Tracker'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<void> _initAdMob() {
    return FirebaseAdMob.instance.initialize(appId: AdManager.appId);
  }

  BannerAd _bannerAd;

  void _loadBannerAd() {
    _bannerAd
      ..load()
      ..show(anchorType: AnchorType.bottom);
  }

  @override
  void initState() {
    _bannerAd = BannerAd(
      adUnitId: AdManager.bannerAdUnitId,
      size: AdSize.banner,
    );
    _loadBannerAd();
  }

  @override
  void dispose() {
    _bannerAd?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<void>(
      future: _initAdMob(),
      builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
        return Container(
          padding: EdgeInsets.only(bottom: _bannerAd.size.height.toDouble()),
          child: Scaffold(
            appBar: AppBar(
              title: Text(widget.title),
            ),
            body: GoalList(),
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.create),
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => CreateGoalPage(),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
