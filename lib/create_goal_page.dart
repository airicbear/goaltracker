import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CreateGoalPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create new goal'),
      ),
      body: _CreateGoalForm(),
    );
  }
}

class _CreateGoalForm extends StatefulWidget {
  @override
  _CreateGoalFormState createState() => _CreateGoalFormState();
}

class _CreateGoalFormState extends State<_CreateGoalForm> {
  final _formKey = GlobalKey<FormState>();
  final _titleController = TextEditingController();
  final _dateController = TextEditingController();
  DateTime _date;
  CollectionReference _goals = Firestore.instance.collection('goals');

  @override
  void initState() {
    super.initState();
    _date = DateTime.now().add(Duration(days: 1));
    _dateController.text = _date.toIso8601String();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _dateController.dispose();
    super.dispose();
  }

  Future<void> _addGoal() {
    return _goals.add({
      'title': _titleController.value.text,
      'date': DateTime.tryParse(_dateController.value.text) != null
          ? _dateController.value.text
          : DateTime.now().toIso8601String(),
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        children: [
          TextFormField(
            textCapitalization: TextCapitalization.sentences,
            controller: _titleController,
            decoration: InputDecoration(
              hintText: 'Title',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter a title for your goal.';
              }
              return null;
            },
          ),
          TextFormField(
            readOnly: true,
            controller: _dateController,
            decoration: InputDecoration(
              hintText: 'Date',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter a date for your goal.';
              }
              if (DateTime.tryParse(value) == null) {
                return 'Invalid format.';
              }
              return null;
            },
            onTap: () async {
              DateTime date = await showDatePicker(
                context: context,
                initialDate: DateTime.now().add(Duration(days: 1)),
                firstDate: DateTime.now().subtract(Duration(days: 365)),
                lastDate: DateTime.now().add(Duration(days: 18250)),
              );

              TimeOfDay time;
              if (date != null) {
                time = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.now(),
                );
              }

              if (date != null && time != null) {
                setState(() {
                  String _dateString = date
                      .add(
                        Duration(
                          hours: time.hour,
                          minutes: time.minute,
                        ),
                      )
                      .toIso8601String();
                  _dateController.value = TextEditingValue(
                    text: _dateString,
                  );
                  _date = DateTime.parse(_dateString);
                });
              }
            },
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              "${MaterialLocalizations.of(context).formatFullDate(
                _date,
              )} at ${MaterialLocalizations.of(context).formatTimeOfDay(TimeOfDay.fromDateTime(_date))}",
              style: TextStyle(fontSize: 16.0),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              _addGoal();
              if (_formKey.currentState.validate()) {
                Navigator.of(context).pop();
              }
            },
            child: Text('Create goal'),
          ),
        ],
      ),
    );
  }
}
