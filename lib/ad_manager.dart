import 'dart:io';

class AdManager {
  static String get appId {
    if (Platform.isAndroid) {
      return "ca-app-pub-3940256099942544~4354546703";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      // ca-app-pub-2754960959595614/8177149945
      return "ca-app-pub-3940256099942544/8865242552";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }
}
